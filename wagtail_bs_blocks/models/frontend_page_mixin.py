from django.forms import Media
from wagtail.core.blocks import StreamValue


class FrontendPageMixin():

    def frontend_media(self):
        """
        build up a media object from all page fields
        that contain
        """
        media = Media()
        # xray = [getattr(self, x[0]) for x in vars(self).items() if type(x[1]) is StreamValue]
        for properties in vars(self).items():
            if type(properties[1]) is StreamValue:
                for block_val in getattr(self, properties[0]):
                    if hasattr(block_val.block, 'frontend_media'):
                        media += block_val.block.frontend_media
        return media
