from operator import attrgetter
from django.utils.translation import gettext_lazy as _
from wagtail.core.blocks import StructBlock, CharBlock
from .header_block import HeaderBlock

"""
embed the header block and pass the page field as text
"""
class TitleHeaderBlock(HeaderBlock):

    class Meta:
        help_text = _('Page field as header text or use header text if supplied.')

    header_text = CharBlock(
        label=_('Header Text'),
        max_length=250,
        required=False
    )

    # the page field to pass as header text
    # can be dotted attribute path like obj.attr
    field_name=None
    ctx_name=None

    def __init__(self, field_name='title', ctx_name='page', **kwargs):
        self.field_name = field_name
        self.ctx_name = ctx_name
        super().__init__(**kwargs)

    def get_context(self, value, parent_context=None):
        ctx = super().get_context(value, parent_context=parent_context)
        if not ctx['value']['header_text']:
            ctx['value']['header_text'] = attrgetter(self.field_name)(ctx[self.ctx_name])
        return ctx
