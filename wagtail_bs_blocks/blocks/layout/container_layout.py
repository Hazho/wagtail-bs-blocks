from django.utils.translation import gettext_lazy as _
from wagtail.core.blocks import (
    ChoiceBlock,
    CharBlock,
)
from wagtail.images.blocks import ImageChooserBlock
from wagtail_bs_blocks import settings
from ..base_blocks import BaseLayout
from ..background_style import BackgroundStyle


class ContainerLayout(BaseLayout):

    class Meta:
        verbose_name = _('Container Layout')
        template = 'wagtail_bs_blocks/layout/container_layout.html'

    container_fluid = ChoiceBlock(
        choices=(
            ('fluid', 'fluid'),
            ('sm', 'sm'),
            ('md', 'md'),
            ('lg', 'lg'),
            ('xl', 'xl'),
        ),
        required=False,
        label=_('Container width'),
        help_text=_('container-breakpoint'),
    )

    container_class = CharBlock(
        required=False,
        label=('Container class'),
        max_length=settings.CLASS_MAX_LENGTH
    )

    container_id = CharBlock(
        required=False,
        label=('Container id'),
        max_length=255
    )
