from django.utils.translation import gettext_lazy as _
from wagtail.core.blocks import (
    ChoiceBlock,
    CharBlock,
)
from ..base_blocks import BaseLayout
from .container_layout import ContainerLayout
from wagtail_bs_blocks import settings


class GridColumn(BaseLayout):

    class Meta:
        label = 'Grid Column'
        icon = 'pick'
        template = 'wagtail_bs_blocks/layout/grid_column.html'

    column_size = ChoiceBlock(
        choices=(
            ('sm', 'sm'),
            ('md', 'md'),
            ('lg', 'lg'),
            ('xl', 'xl'),
        ),
        required=False,
        label=_('Column size'),
    )

    column_number = ChoiceBlock(
        choices=(
            ('1', '1'),
            ('2', '2'),
            ('3', '3'),
            ('4', '4'),
            ('5', '5'),
            ('6', '6'),
        ),
        required=False,
        label=_('Column number'),
    )

    grid_col_class = CharBlock(
        required=False,
        label=('Grid col class'),
        max_length=settings.CLASS_MAX_LENGTH
    )



class GridRow(BaseLayout):

    class Meta:
        icon = 'cogs'
        label = _('Grid Layout')
        template = 'wagtail_bs_blocks/layout/grid_row.html'

    grid_row_class = CharBlock(
        required=False,
        label=('Grid Row class'),
        max_length=settings.CLASS_MAX_LENGTH
    )

    def __init__(self, local_blocks=None, **kwargs):

        super().__init__(
            local_blocks=[
                ('grid_row', GridColumn(local_blocks))
            ]
        )


class GridContainer(ContainerLayout):

    class Meta:
        icon = 'placeholder'
        label = _('Grid Container')
        help_text = 'https://getbootstrap.com/docs/4.4/layout/grid/'
        template = 'wagtail_bs_blocks/layout/container_layout.html'

    def __init__(self, local_blocks=None, **kwargs):

        super().__init__(
            local_blocks=[
                ('grid_container', GridRow(local_blocks))
            ]
        )
