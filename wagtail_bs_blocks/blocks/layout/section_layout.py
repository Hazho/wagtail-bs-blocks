from django.utils.translation import gettext_lazy as _
from wagtail.core.blocks import (
    ChoiceBlock,
)
from ..base_blocks import BaseLayout
from .container_layout import ContainerLayout
from ..header_block import HeaderBlock


class SectionContent(BaseLayout):

    class Meta:
        label = 'Section'
        icon = 'pick'
        template = 'wagtail_bs_blocks/layout/section_content.html'


class SectionLayout(BaseLayout):

    class Meta:
        icon = 'cogs'
        label = _('Section Layout')
        template = 'wagtail_bs_blocks/layout/section_layout.html'
        help_text = _('Example: <section><h1></h1><div...</section>')

    section_type = ChoiceBlock(
        choices=(
            ('section', 'Section'),
            ('article', 'Article'),
            ('aside', 'Aside'),
            ('nav', 'Navigation'),
        ),
        default='Section',
        required=True,
        label=_('Section Type'),
    )

    section_header = HeaderBlock()

    def __init__(self, local_blocks=None, **kwargs):
        super().__init__(
            local_blocks=[
                ('section_layout', SectionContent(local_blocks))
            ]
        )


class SectionContainer(ContainerLayout):

    class Meta:
        icon = 'placeholder'
        label = _('Section Container')
        template = 'wagtail_bs_blocks/layout/container_layout.html'
        help_text = _('Example: <section><h1></h1><div...</section>')

    def __init__(self, local_blocks=None, **kwargs):
        super().__init__(
            local_blocks=[
                ('section_container', SectionLayout(local_blocks))
            ]
        )
