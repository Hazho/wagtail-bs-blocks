from django.utils.translation import gettext_lazy as _
from wagtail.core.blocks import ChoiceBlock
from .container_layout import ContainerLayout
from ..base_blocks import BaseLayout


class CardContent(BaseLayout):

    class Meta:
        label = 'Cards'
        icon = 'pick'
        template = 'wagtail_bs_blocks/cards/card_content.html'


class CardLayout(BaseLayout):

    class Meta:
        label = _('Card Layout')
        help_text = _('Group or Deck layout')
        template = 'wagtail_bs_blocks/cards/card_layout.html'
        icon = 'cogs'

    layout_type = ChoiceBlock(
        choices=(
            ('group', 'Group'),
            ('deck', 'Deck'),
            ('columns', 'Columns'),
        ),
        required=True,
        default='group',
        label=_('Card layout type'),
        help_text=_('https://getbootstrap.com/docs/4.4/components/card/#card-groups'),
    )

    def __init__(self, local_blocks=None, **kwargs):
        super().__init__(
            local_blocks=[
                ('card_layout', CardContent(local_blocks))
            ]
        )


class CardContainer(ContainerLayout):

    class Meta:
        label = _('Card Container')
        help_text = _('Group or Deck layout')
        template = 'wagtail_bs_blocks/layout/container_layout.html'
        icon = 'placeholder'

    def __init__(self, local_blocks=None, **kwargs):
        super().__init__(
            local_blocks=[
                ('card_container', CardLayout(local_blocks))
            ]
        )
