from django.utils.translation import gettext_lazy as _
from wagtail.core.blocks import (
        StructBlock,
        IntegerBlock,
        CharBlock
)
from wagtail.images.blocks import ImageChooserBlock
from wagtail_bs_blocks import settings


class BackgroundStyle(StructBlock):

    class Meta:
        verbose_name = _('Background Style')

    image = ImageChooserBlock(
        required=False,
        max_length=255,
        label=_('style:background-image'),
    )

    height = IntegerBlock(
            required=False,
            default=100,
            label=_('style:height vh=viewport height'),
            )

    background_color_class = CharBlock(
        required=False,
        label=('background_color_class'),
        max_length=settings.CLASS_MAX_LENGTH,
        default='mask',
    )

    background_color = CharBlock(
        required=False,
        label=('background-color'),
        default='rgba(0, 0, 0, 0)',
        help_text=_('rgba(0, 0, 0, 0)'),
    )

