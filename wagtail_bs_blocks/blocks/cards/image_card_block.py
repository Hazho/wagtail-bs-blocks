from django.utils.translation import gettext_lazy as _
from wagtail.core.blocks import CharBlock
from wagtail.images.blocks import ImageChooserBlock
from .base_card_block import BaseCardBlock
from ..image_block import ImageBlock

class ImageCardBlock(BaseCardBlock):

    class Meta:
        help_text = _('Image and text in a card.')
        icon = 'image'
        template = 'wagtail_bs_blocks/cards/image_card_block.html'

    image_block = ImageBlock()
