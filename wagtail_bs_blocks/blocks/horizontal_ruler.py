from django.utils.translation import gettext_lazy as _
from wagtail.core.blocks import StaticBlock
from django.utils.safestring import mark_safe


class HorizontalRuler(StaticBlock):
    class Meta:
        icon = 'horizontalrule'
        label = _('Horizontal Ruler')

    def render_basic(self, value, context=None):
        return mark_safe('<hr/>')
