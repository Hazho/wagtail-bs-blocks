from django.utils.translation import gettext_lazy as _
from wagtail.core import blocks
from wagtail.core.blocks import RichTextBlock


SIMPLERICHTEXT_FEATURES = ["link", "bold", "italic", "ol", "ul", "document-link"]


class SimpleRichTextBlock(RichTextBlock):
    """
    Custom block inheriting from Wagtail's original one but replacing the RichText by a SimpleRichText
    """
    def __init__(self, *args, **kwargs):
        super().__init__(*args, features=SIMPLERICHTEXT_FEATURES, **kwargs)

    class Meta:
        icon = 'bold'
