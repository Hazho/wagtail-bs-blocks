from django.utils.translation import gettext_lazy as _
from wagtail.core.blocks import (
    StructBlock,
    StreamBlock,
)


class BaseLayout(StructBlock):

    def __init__(self, local_blocks=None, **kwargs):
        meta_label = _('Content')

        if hasattr(self.__class__, '_meta_class'):
            meta_label = getattr(self.__class__, '_meta_class').label

        if not local_blocks:
            local_blocks = ()

        if local_blocks:
            local_blocks = (('content', StreamBlock(local_blocks, label=meta_label)),)

        super().__init__(local_blocks, **kwargs)
