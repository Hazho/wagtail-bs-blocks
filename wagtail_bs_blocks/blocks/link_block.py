from django.utils.translation import gettext_lazy as _
from django.core.validators import RegexValidator
from wagtail.documents.blocks import DocumentChooserBlock
from wagtail.core.blocks import (
    StructValue,
    StructBlock,
    CharBlock,
    PageChooserBlock,
    URLBlock,
    EmailBlock,
    RegexBlock,
)


class LinkStructValue(StructValue):

    @property
    def url(self):
        page_link = self.get('page_link')
        if page_link:
            return page_link.url

        doc_link = self.get('doc_link')
        if doc_link:
            return doc_link.url

        external_link = self.get('external_link')
        if external_link:
            return external_link

        anchor_link = self.get('anchor_link')
        if anchor_link:
            return '#{}'.format(anchor_link)

        mailto_link = self.get('mailto_link')
        if mailto_link:
            return 'mailto:{}'.format(mailto_link)

        phone_number_link = self.get('phone_number_link')
        if phone_number_link:
            return 'tel:{}'.format(phone_number_link)


class BaseLinkBlock(StructBlock):

    class Meta:
        value_class = LinkStructValue
        form_classname = 'evenlike'

    page_link = PageChooserBlock(
        label=_('Page link'),
        required=False
    )

    doc_link = DocumentChooserBlock(
        label=_('Document link'),
        required=False,
    )

    external_link = URLBlock(
        label=_('External link'),
        required=False
    )

    anchor_link = CharBlock(
        required=False,
        max_length=255,
        label=_('Anchor link'),
        help_text='#anchor'
    )

    mailto_link = EmailBlock(
        required=False,
        label=_('Mailto link'),
        help_text='mailto:email'
    )

    phone_number_link = RegexBlock(
        required=False,
        regex=r'^\+?1?\d{9,15}$',
        label=_('Phone link'),
        help_text='tel:+999999999',
        error_message="Phone number must be entered in the format: \
        '+999999999'. Up to 15 digits allowed.",
        max_length=17
    )

    link_class = CharBlock(
        required=False,
        max_length=255,
        label=_('Link Class'),
    )

    link_rel = CharBlock(
        required=False,
        max_length=255,
        label=_('rel'),
        help_text=_('The relationship of the linked URL'),
    )

    link_target = CharBlock(
        required=False,
        max_length=255,
        label=_('target'),
        help_text=_('Where to display the linked URL. _self, _blank, parent, _top'),
    )
