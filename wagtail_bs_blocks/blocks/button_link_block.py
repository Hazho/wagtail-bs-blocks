from django.utils.translation import gettext_lazy as _
from wagtail.core.blocks import ChoiceBlock
from .base_blocks import BaseLayout
from .link_block import BaseLinkBlock, LinkStructValue


class ButtonStyle(ChoiceBlock):

    class Meta:
        label = _('Button style')

    choices = (
        ('primary', 'Primary'),
        ('secondary', 'Secondary'),
        ('warning', 'Warning'),
        ('danger', 'Danger'),
        ('success', 'Success'),
    )


class ButtonSize(ChoiceBlock):

    class Meta:
        label = _('Button size')

    choices = (
        ('Normal', 'btn'),
        ('Small', 'btn-sm'),
        ('Large', 'btn-lg'),
        ('Block', 'btn-block'),
    )


class ButtonLinkBlock(BaseLayout):

    class Meta:
        label = _('Button Link')
        template = 'wagtail_bs_blocks/button_link_block.html'
        icon = 'link'
        value_class = LinkStructValue

    button_link = BaseLinkBlock()

    button_style = ButtonStyle(
        required=False,
    )
