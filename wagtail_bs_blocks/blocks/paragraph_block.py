from django.utils.translation import gettext_lazy as _
from wagtail.core.blocks import TextBlock, CharBlock, StructBlock 
from wagtail_bs_blocks import settings 


class ParagraphBlock(StructBlock):

    class Meta:
        template = 'wagtail_bs_blocks/paragraph_block.html'
        icon = "bold"

    paragraph_class = CharBlock(
        required=False,
        label=('Paragraph class'),
        max_length=settings.CLASS_MAX_LENGTH,
        default='text-center',
    )

    text = TextBlock(label=_('Paragraph'))
