from django.utils.translation import gettext_lazy as _
from wagtail.core.blocks import CharBlock
from wagtail_bs_blocks import settings
from .base_blocks import BaseLayout
from .simple_richtext_block import SimpleRichTextBlock


class FigureBlock(BaseLayout):

    class Meta:
        label = _('Figure')
        template = 'wagtail_bs_blocks/figure_block.html'
        icon = 'cross'

    '''
    figure - one or more image, richtext
    figcaption - zero or one richtext
    '''
    figure_caption = SimpleRichTextBlock(
        required=False,
        label=_('Figure Caption'),
    )

    figure_class = CharBlock(
        required=False,
        label=('Figure class'),
        max_length=settings.CLASS_MAX_LENGTH,
        default='figure',
    )

    figcaption_class = CharBlock(
        required=False,
        label=('Figcaption class'),
        max_length=settings.CLASS_MAX_LENGTH,
        default='figure-caption',
    )
