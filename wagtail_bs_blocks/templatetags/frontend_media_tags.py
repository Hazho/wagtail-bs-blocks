from django import template
from ..models.frontend_page_mixin import FrontendPageMixin


register = template.Library()


@register.simple_tag
def frontend_media(frontend_mixin, media='css'):
    if issubclass(frontend_mixin.__class__, FrontendPageMixin):
        # check page type is FrontendPageMixin
        return frontend_mixin.frontend_media()[media]
    return ''
