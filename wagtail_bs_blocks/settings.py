import sys


class Settings(object):
    defaults = {
        'CLASS_MAX_LENGTH': ('WAGTAIL_BS_BLOCKS_CLASS_MAX_LENGTH', 255),
        'EXTRA_BLOCKS' : ('WAGTAIL_BS_BLOCKS_EXTRA_BLOCKS', []),
    }

    def __getattr__(self, attribute):
        from django.conf import settings
        if attribute in self.defaults:
            return getattr(settings, *self.defaults[attribute])


sys.modules[__name__] = Settings()
